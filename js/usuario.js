$(document).ready(function(){

    consultarUsuarios();

    $('#cbEmpresa').on('change', function(){
        consultarAreas($(this).val());
    });

    $('#cbArea').on('change', function(){
        consultarCargos($(this).val());
    });

    $('#frmRegistro').on('submit', function(e){
        e.preventDefault();
        if($('#password').val() == '' && $('#hdId').val() == ''){
            alert('Debe ingresar la contraseña');
        }
        let form = $(this)[0];
        let datos = new FormData(form);
        datos.append('accion', 'guardarUsuario');
        axios.post(`/ajax/usuario.ajax.php`, datos).then(res => {
            form.reset();
            consultarUsuarios();
        });
    });

});

function consultarAreas(empresa, valor=0){
    $('#cbArea option').remove();
    $('#cbCargo option').remove();
    $('#cbCargo').append(`<option value="">Seleccione..</option>`);
    axios.get(`/ajax/area.ajax.php?accion=consultarAreas&empresa=${empresa}`).then(res => {
        let areas = res.data;
        if(areas.length > 0){
            $('#cbArea').append(`<option value="">Seleccione..</option>`);
            for(area of areas){
                $('#cbArea').append(`<option value="${area.area_id}">${area.area_descripcion}</option>`);
            }
            if(valor != 0){
                $('#cbArea').val(valor);
            }
        }else{
            $('#cbArea').append(`<option value="">No hay información</option>`);
        }
    });
}

function consultarCargos(area, valor=0){
    $('#cbCargo option').remove();
    axios.get(`/ajax/cargo.ajax.php?accion=consultarCargos&area=${area}`).then(res => {
        let cargos = res.data;
        if(cargos.length > 0){
            $('#cbCargo').append(`<option value="">Seleccione..</option>`);
            for(cargo of cargos){
                $('#cbCargo').append(`<option value="${cargo.cargo_id}">${cargo.cargo_descripcion}</option>`);
            }
            if(valor != 0){
                $('#cbCargo').val(valor);
            }
        }else{
            $('#cbCargo').append(`<option value="">No hay información</option>`);
        }
    });
}

function consultarUsuarios(){
    $('#tbUsuarios tr').remove();
    axios.get('/ajax/usuario.ajax.php/?accion=consultarUsuarios').then(res => {
        let usuarios = res.data;
        if(usuarios.length > 0){
            for(usuario of usuarios){
                $('#tbUsuarios').append(`<tr>
                                            <td>${usuario.nombre}</td>
                                            <td>${usuario.email}</td>
                                            <td>${usuario.cargo_descripcion}</td>
                                            <td>${usuario.area_descripcion}</td>
                                            <td>${usuario.razon_social}</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" onclick="editar(${usuario.usuario_id})">Editar</button>
                                                <button type="button" class="btn btn-danger" onclick="eliminar(${usuario.usuario_id})">Eliminar</button>
                                            </td>
                                        </tr>`);
            }
        }
    });
}

function editar(id){
    axios.get(`/ajax/usuario.ajax.php?accion=consultarId&id=${id}`).then(res => {
        let usuario = res.data;
        if(usuario){
            $('#hdId').val(usuario.usuario_id);
            $('#txtNombre').val(usuario.nombre);
            $('#txtEmail').val(usuario.email);
            $('#cbEmpresa').val(usuario.empresa_id);
            consultarAreas(usuario.empresa_id,usuario.area_id);
            consultarCargos(usuario.area_id, usuario.cargo);
        }
    });
}

function eliminar(id){
    let datos = new FormData();
    datos.append('id', id);
    datos.append('accion', 'eliminarUsuario');
    axios.post('/ajax/usuario.ajax.php', datos).then(res => {
        consultarUsuarios();
    });
}