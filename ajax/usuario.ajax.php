<?php

require_once '../class/usuario.php';

if(isset($_GET['accion'])){
    if($_GET['accion'] == 'consultarUsuarios'){
        $res = Usuario::consultar();
        echo json_encode($res);
    }

    if($_GET['accion'] == 'consultarId'){
        $res = Usuario::consultarId($_GET['id']);
        echo json_encode($res);
    }
}

if(isset($_POST['accion'])){
    if($_POST['accion'] == 'guardarUsuario'){
        if($_POST['id'] != ''){
            $res = Usuario::editar($_POST);
        }else{
            $res = Usuario::guardar($_POST);
        }
        echo json_encode($res);
    }

    if($_POST['accion'] == 'eliminarUsuario'){
        $res = Usuario::eliminar($_POST['id']);
        echo json_encode($res);
    }
}
