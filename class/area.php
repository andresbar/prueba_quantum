<?php

require_once 'conexion.php';

Class Area {


    static function consultar($empresa){
        $con = new DB();
        $stmt = $con->db->prepare('select * from area where empresa_id = ?');
        $stmt->bind_param('i', $empresa);
        $stmt->execute();
        $result = $stmt->get_result();
        $res = [];
        while($row = $result->fetch_assoc()){
            $res[] = $row;
        }
        return $res;
    }

}