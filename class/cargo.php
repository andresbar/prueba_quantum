<?php

require_once 'conexion.php';

Class Cargo {


    static function consultar($area){
        $con = new DB();
        $stmt = $con->db->prepare('select * from cargo where area_id = ?');
        $stmt->bind_param('i', $area);
        $stmt->execute();
        $result = $stmt->get_result();
        $res = [];
        while($row = $result->fetch_assoc()){
            $res[] = $row;
        }
        return $res;
    }

}