<?php

require_once 'conexion.php';

Class Usuario{

    static function guardar($request){
        $con = new DB();
        $stmt = $con->db->prepare('insert into usuario values(null, ?, ?, ? ,?)');
        $stmt->bind_param('sssi', $request['nombre'], $request['email'], sha1($request['password']), $request['cargo']);
        $stmt->execute();

        return ['Msj' => 'Se ha guardado el usuario exitosamente.'];
    }

    static function consultar(){
        $con = new DB();
        $sql = 'select u.*, c.cargo_descripcion, a.area_descripcion, e.razon_social
                from usuario u
                inner join cargo c on u.cargo=c.cargo_id
                inner join area a on c.area_id=a.area_id
                inner join empresa e on a.empresa_id=e.empresa_id';
        $usuarios = $con->db->query($sql);
        $res = [];
        while($row = $usuarios->fetch_assoc()){
            $res[] = $row;
        }
        return $res;
    }

    static function consultarId($id){
        $con = new DB();
        $sql = 'select u.*,a.area_id, e.empresa_id
                from usuario u
                inner join cargo c on u.cargo=c.cargo_id
                inner join area a on c.area_id=a.area_id
                inner join empresa e on a.empresa_id=e.empresa_id
                where u.usuario_id='.$id;
        $usuario = $con->db->query($sql);
        $res = $usuario->fetch_assoc();
        return $res;
    }

    static function editar($request){
        $con = new DB();
        if($request['password'] != ''){
            $sql = 'update usuario set nombre=?, email=?, cargo=?, password=? where usuario_id=?';
            $stmt = $con->db->prepare($sql);
            $stmt->bind_param('ssisi', $request['nombre'], $request['email'], $request['cargo'], sha1($request['password']), $request['id']);
        }else{
            $sql = 'update usuario set nombre=?, email=?, cargo=? where usuario_id=?';
            $stmt = $con->db->prepare($sql);
            $stmt->bind_param('ssii', $request['nombre'], $request['email'], $request['cargo'], $request['id']);
        }
        $stmt->execute();

        return ['Msj' => 'Se ha actualizado el usuario exitosamente.'];
    }

    static function eliminar($id){
        $con = new DB();
        $stmt = $con->db->prepare('delete from usuario where usuario_id=?');
        $stmt->bind_param('i', $id);
        $stmt->execute();

        return ['Msj' => 'Se ha eliminado el usuario exitosamente.'];
    }
}